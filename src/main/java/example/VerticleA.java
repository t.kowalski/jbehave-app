package example;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Verticle;

/**
 * @author Tobiasz Kowalski (t.kowalski@oberthur.com) on 13/04/18.
 */
public class VerticleA extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        super.start();
        System.out.println("VerticleA started");
    }
}
