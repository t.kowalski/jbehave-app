package example;

import io.vertx.core.Vertx;

/**
 * @author Tobiasz Kowalski (t.kowalski@oberthur.com) on 13/04/18.
 */
public class Test {

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new VerticleA());
        vertx.deployVerticle(new VerticleB());
    }
}
