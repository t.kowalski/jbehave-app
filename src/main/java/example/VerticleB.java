package example;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import static io.netty.handler.codec.rtsp.RtspHeaders.Names.CONTENT_TYPE;
import static io.vertx.core.json.Json.encodePrettily;
import static io.vertx.ext.web.Router.router;

/**
 * @author Tobiasz Kowalski (t.kowalski@oberthur.com) on 13/04/18.
 */
public class VerticleB extends AbstractVerticle {
    private static final String APPLICATION_JSON = "application/json";


    @Override
    public void start(Future<Void> startFuture) throws Exception {
        vertx.createHttpServer()
                .requestHandler(createRoute()::accept)
                .listen(8080, result -> {
                    if (result.succeeded()) {
                        startFuture.complete();
                    } else {
                        startFuture.fail(result.cause());
                    }
                });
    }

    private Router createRoute() {
        Router router = router(vertx);
        router.get("/hello/:name").handler(this::helloHandler);
        //TODO add health check endpoint from <artifactId>vertx-health-check</artifactId>
        return router;
    }

    private void helloHandler(RoutingContext routingContext) {
        String name = routingContext.request().getParam("name");
        routingContext.response()
                .putHeader(CONTENT_TYPE, APPLICATION_JSON)
                .setStatusCode(200)
                .end(encodePrettily(new JsonObject().put("name", name)));
    }

}
