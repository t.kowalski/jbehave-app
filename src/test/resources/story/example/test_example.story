Meta:

Narrative:
As a Mr Tomato
I want to see greatings from this awsome app
So that I can achieve a great example of cooperation between serenity and vertx

Scenario: Greating from the app
Given a running system
When Mr tomato asks for greatings with his name <tomato_name>
Then Response code should be 200
And Response message should contain JSON with field 'name' having value <tomato_name>
Examples:
|tomato_name|
|Tobi|
|Bobi|
|Obi kenobi|
|To bi or not To bi|


Scenario: Not given greating from the app
Given a running system
When Mr tomato asks for greatings with the name <tomato_name> on <address>
Then Response code should be 404
Examples:
|tomato_name|address|
|Tobi|dejmnieto|
|Bobi|hultaj|
|Obi kenobi|szelma|
|To bi or not To bi|cycki|