package example;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.jbehave.SerenityStory;
import net.serenitybdd.rest.SerenityRest;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import static org.hamcrest.CoreMatchers.equalTo;

/**
 * @author Tobiasz Kowalski (t.kowalski@oberthur.com) on 13/04/18.
 */
public class TestExample extends SerenityStory {

    @Given("a running system")
    public void givenARunningSystem() {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(VerticleA.class.getName());
        vertx.deployVerticle(VerticleB.class.getName());
        //TODO call health check endpoint
    }

    @When("Mr tomato asks for greatings with his name $tomato_name")
    public void whenMrTomatoAsksForGreatingsWithHisName(@Named("tomato_name") String name) {
        Response response = SerenityRest.rest()
                .when()
                .contentType("application/json; charset=utf-8")
                .body(new JsonObject()
                        .put("test", "dupa")
                        .encodePrettily())
                .get("/hello/" + name);

        Serenity.setSessionVariable("response").to(response);
    }

    @When("Mr tomato asks for greatings with the name $tomato_name on $address")
    public void whenMrTomatoAsksForGreatingsWithHisNameOnAddress(@Named("tomato_name") String name, @Named("address") String address) {
        Response response = SerenityRest.rest()
                .when()
                .contentType("application/json; charset=utf-8")
                .body(new JsonObject()
                        .put("test", "dupa")
                        .encodePrettily())
                .get(address + name);

        Serenity.setSessionVariable("response").to(response);
    }

    @Then("Response code should be $code")
    public void thenResponseCodeShouldBe(String code) {
        Response response = Serenity.sessionVariableCalled("response");
        response.then()
                .statusCode(Integer.valueOf(code));
    }

    @Then("Response message should contain JSON with field 'name' having value $tomato_name")
    public void thenResponseMessageShouldContainJSONWithFieldnameHavingValue(@Named("tomato_name") String name) {
        Response response = Serenity.sessionVariableCalled("response");
        response.then()
                .contentType(ContentType.JSON)
                .body("name", equalTo(name));
    }

}
